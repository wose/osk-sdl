#ifndef YKEY_H
#define YKEY_H

#include <string>
#include <ykcore.h>


class YubiKey {
public:
    YubiKey();
    ~YubiKey();

    std::string challenge(const std::string& challenge);

    int challenges = 0;
private:
    YK_KEY *yk = nullptr;
};
#endif // YKEY_H
