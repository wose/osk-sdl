#include "sha256.h"

#include <yubikey.h>
#include <ykdef.h>

#include "ykey.h"
YubiKey::YubiKey() {
    if(!yk_init()) {
        fprintf(stderr, "Failed to initialize yubikey");
        exit(EXIT_FAILURE);
    }

    yk = yk_open_key(0);

    if(!yk) {
        fprintf(stderr, "Failed to open yubikey");
        exit(EXIT_FAILURE);
    }
}

YubiKey::~YubiKey() {
    if(yk && !yk_close_key(yk))
        fprintf(stderr, "Failed to close yubikey");
    if(!yk_release())
        fprintf(stderr, "Failed to release yubikey");
}

std::string YubiKey::challenge(const std::string& challenge) {
    challenges++;
    unsigned char res[SHA1_MAX_BLOCK_SIZE];
    unsigned char out[(SHA1_MAX_BLOCK_SIZE * 2 + 1)];

    auto hash = SHA256::hashString(challenge);

    if(!yk_challenge_response(yk,
                              SLOT_CHAL_HMAC2,
                              true,
                              hash.length(),
                              reinterpret_cast<const unsigned char*>(hash.c_str()),
                              sizeof(res),
                              res))
    {
        fprintf(stderr, "Failed yubikey challenge response");
    }

    yubikey_hex_encode((char *) out, (char *) res, 20);

    return std::string(reinterpret_cast<const char*>(out));
}
